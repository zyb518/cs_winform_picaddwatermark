﻿namespace csharp_图片水印工具
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btn_file_add = new System.Windows.Forms.Button();
            this.btn_folder_add = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lb_files = new System.Windows.Forms.ListBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.拍摄日期 = new System.Windows.Forms.TabPage();
            this.btn_adddate_preview = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_photo_indate = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_pic_addshootdate = new System.Windows.Forms.Button();
            this.文字水印 = new System.Windows.Forms.TabPage();
            this.btn_pic_preview = new System.Windows.Forms.Button();
            this.btn_date = new System.Windows.Forms.Button();
            this.btn_add_water = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.图片水印 = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.tb_waterpic_rate = new System.Windows.Forms.TextBox();
            this.btn_waterpic_decrease = new System.Windows.Forms.Button();
            this.btn_waterpic_increase = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_addpic_preview = new System.Windows.Forms.Button();
            this.btn_waterpic_select = new System.Windows.Forms.Button();
            this.btn_add_pic_water = new System.Windows.Forms.Button();
            this.pictureBox_waterpic = new System.Windows.Forms.PictureBox();
            this.btn_color = new System.Windows.Forms.Button();
            this.btn_font = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslb_1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.btn_clear_files = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.openFileDialog_waterpic = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tb_filename_pretext = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chb_overwrite = new System.Windows.Forms.CheckBox();
            this.chb_addrename = new System.Windows.Forms.CheckBox();
            this.btn_font_increase = new System.Windows.Forms.Button();
            this.btn_font_decrease = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.tb_font_size = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_right_down = new System.Windows.Forms.RadioButton();
            this.rb_right_up = new System.Windows.Forms.RadioButton();
            this.rb_left_down = new System.Windows.Forms.RadioButton();
            this.rb_left_up = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tb_y_position = new System.Windows.Forms.TextBox();
            this.tb_x_position = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.拍摄日期.SuspendLayout();
            this.文字水印.SuspendLayout();
            this.图片水印.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_waterpic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_file_add
            // 
            this.btn_file_add.Location = new System.Drawing.Point(12, 27);
            this.btn_file_add.Name = "btn_file_add";
            this.btn_file_add.Size = new System.Drawing.Size(75, 23);
            this.btn_file_add.TabIndex = 0;
            this.btn_file_add.Text = "添加文件";
            this.btn_file_add.UseVisualStyleBackColor = true;
            this.btn_file_add.Click += new System.EventHandler(this.btn_file_add_Click);
            // 
            // btn_folder_add
            // 
            this.btn_folder_add.Location = new System.Drawing.Point(12, 56);
            this.btn_folder_add.Name = "btn_folder_add";
            this.btn_folder_add.Size = new System.Drawing.Size(75, 23);
            this.btn_folder_add.TabIndex = 1;
            this.btn_folder_add.Text = "添加文件夹";
            this.btn_folder_add.UseVisualStyleBackColor = true;
            this.btn_folder_add.Click += new System.EventHandler(this.btn_folder_add_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Multiselect = true;
            // 
            // lb_files
            // 
            this.lb_files.FormattingEnabled = true;
            this.lb_files.ItemHeight = 12;
            this.lb_files.Location = new System.Drawing.Point(109, 27);
            this.lb_files.Name = "lb_files";
            this.lb_files.ScrollAlwaysVisible = true;
            this.lb_files.Size = new System.Drawing.Size(571, 112);
            this.lb_files.TabIndex = 2;
            this.lb_files.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lb_files_MouseDoubleClick);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.拍摄日期);
            this.tabControl1.Controls.Add(this.文字水印);
            this.tabControl1.Controls.Add(this.图片水印);
            this.tabControl1.Location = new System.Drawing.Point(761, 27);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(200, 275);
            this.tabControl1.TabIndex = 3;
            // 
            // 拍摄日期
            // 
            this.拍摄日期.Controls.Add(this.btn_adddate_preview);
            this.拍摄日期.Controls.Add(this.label4);
            this.拍摄日期.Controls.Add(this.tb_photo_indate);
            this.拍摄日期.Controls.Add(this.label3);
            this.拍摄日期.Controls.Add(this.btn_pic_addshootdate);
            this.拍摄日期.Location = new System.Drawing.Point(4, 22);
            this.拍摄日期.Name = "拍摄日期";
            this.拍摄日期.Padding = new System.Windows.Forms.Padding(3);
            this.拍摄日期.Size = new System.Drawing.Size(192, 249);
            this.拍摄日期.TabIndex = 2;
            this.拍摄日期.Text = "拍摄日期";
            this.拍摄日期.UseVisualStyleBackColor = true;
            // 
            // btn_adddate_preview
            // 
            this.btn_adddate_preview.Location = new System.Drawing.Point(10, 180);
            this.btn_adddate_preview.Name = "btn_adddate_preview";
            this.btn_adddate_preview.Size = new System.Drawing.Size(61, 55);
            this.btn_adddate_preview.TabIndex = 7;
            this.btn_adddate_preview.Text = "预览";
            this.btn_adddate_preview.UseVisualStyleBackColor = true;
            this.btn_adddate_preview.Click += new System.EventHandler(this.btn_adddate_preview_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "选定照片的日期是：";
            // 
            // tb_photo_indate
            // 
            this.tb_photo_indate.BackColor = System.Drawing.SystemColors.Control;
            this.tb_photo_indate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tb_photo_indate.ForeColor = System.Drawing.Color.Red;
            this.tb_photo_indate.Location = new System.Drawing.Point(8, 90);
            this.tb_photo_indate.Name = "tb_photo_indate";
            this.tb_photo_indate.ReadOnly = true;
            this.tb_photo_indate.Size = new System.Drawing.Size(178, 21);
            this.tb_photo_indate.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(173, 36);
            this.label3.TabIndex = 2;
            this.label3.Text = "    此功能需要相机具有相应的\r\n功能，也就是要包含exif信息,\r\n否则不能实现该效果。";
            // 
            // btn_pic_addshootdate
            // 
            this.btn_pic_addshootdate.Location = new System.Drawing.Point(102, 180);
            this.btn_pic_addshootdate.Name = "btn_pic_addshootdate";
            this.btn_pic_addshootdate.Size = new System.Drawing.Size(75, 55);
            this.btn_pic_addshootdate.TabIndex = 0;
            this.btn_pic_addshootdate.Text = "添加拍摄日期到照片";
            this.btn_pic_addshootdate.UseVisualStyleBackColor = true;
            this.btn_pic_addshootdate.Click += new System.EventHandler(this.btn_pic_addshootdate_Click_1);
            // 
            // 文字水印
            // 
            this.文字水印.Controls.Add(this.btn_pic_preview);
            this.文字水印.Controls.Add(this.btn_date);
            this.文字水印.Controls.Add(this.btn_add_water);
            this.文字水印.Controls.Add(this.richTextBox1);
            this.文字水印.Controls.Add(this.label1);
            this.文字水印.Location = new System.Drawing.Point(4, 22);
            this.文字水印.Name = "文字水印";
            this.文字水印.Padding = new System.Windows.Forms.Padding(3);
            this.文字水印.Size = new System.Drawing.Size(192, 249);
            this.文字水印.TabIndex = 0;
            this.文字水印.Text = "文字水印";
            this.文字水印.UseVisualStyleBackColor = true;
            // 
            // btn_pic_preview
            // 
            this.btn_pic_preview.Location = new System.Drawing.Point(17, 208);
            this.btn_pic_preview.Name = "btn_pic_preview";
            this.btn_pic_preview.Size = new System.Drawing.Size(53, 23);
            this.btn_pic_preview.TabIndex = 9;
            this.btn_pic_preview.Text = "预览";
            this.btn_pic_preview.UseVisualStyleBackColor = true;
            this.btn_pic_preview.Click += new System.EventHandler(this.btn_addtxt_preview_Click);
            // 
            // btn_date
            // 
            this.btn_date.Location = new System.Drawing.Point(111, 8);
            this.btn_date.Name = "btn_date";
            this.btn_date.Size = new System.Drawing.Size(75, 23);
            this.btn_date.TabIndex = 8;
            this.btn_date.Text = "当前日期";
            this.btn_date.UseVisualStyleBackColor = true;
            this.btn_date.Click += new System.EventHandler(this.btn_date_Click);
            // 
            // btn_add_water
            // 
            this.btn_add_water.Location = new System.Drawing.Point(78, 208);
            this.btn_add_water.Name = "btn_add_water";
            this.btn_add_water.Size = new System.Drawing.Size(99, 23);
            this.btn_add_water.TabIndex = 7;
            this.btn_add_water.Text = "添加水印开始";
            this.btn_add_water.UseVisualStyleBackColor = true;
            this.btn_add_water.Click += new System.EventHandler(this.btn_add_water_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.richTextBox1.Location = new System.Drawing.Point(19, 37);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(167, 74);
            this.richTextBox1.TabIndex = 5;
            this.richTextBox1.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "水印文字";
            // 
            // 图片水印
            // 
            this.图片水印.Controls.Add(this.label10);
            this.图片水印.Controls.Add(this.tb_waterpic_rate);
            this.图片水印.Controls.Add(this.btn_waterpic_decrease);
            this.图片水印.Controls.Add(this.btn_waterpic_increase);
            this.图片水印.Controls.Add(this.label9);
            this.图片水印.Controls.Add(this.btn_addpic_preview);
            this.图片水印.Controls.Add(this.btn_waterpic_select);
            this.图片水印.Controls.Add(this.btn_add_pic_water);
            this.图片水印.Controls.Add(this.pictureBox_waterpic);
            this.图片水印.Location = new System.Drawing.Point(4, 22);
            this.图片水印.Name = "图片水印";
            this.图片水印.Padding = new System.Windows.Forms.Padding(3);
            this.图片水印.Size = new System.Drawing.Size(192, 249);
            this.图片水印.TabIndex = 1;
            this.图片水印.Text = "图片水印";
            this.图片水印.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(67, 186);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(11, 12);
            this.label10.TabIndex = 8;
            this.label10.Text = "%";
            // 
            // tb_waterpic_rate
            // 
            this.tb_waterpic_rate.Location = new System.Drawing.Point(23, 181);
            this.tb_waterpic_rate.Name = "tb_waterpic_rate";
            this.tb_waterpic_rate.Size = new System.Drawing.Size(38, 21);
            this.tb_waterpic_rate.TabIndex = 7;
            this.tb_waterpic_rate.Text = "100";
            this.tb_waterpic_rate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_waterpic_rate_KeyPress);
            // 
            // btn_waterpic_decrease
            // 
            this.btn_waterpic_decrease.Location = new System.Drawing.Point(101, 181);
            this.btn_waterpic_decrease.Name = "btn_waterpic_decrease";
            this.btn_waterpic_decrease.Size = new System.Drawing.Size(75, 23);
            this.btn_waterpic_decrease.TabIndex = 6;
            this.btn_waterpic_decrease.Text = "减小";
            this.btn_waterpic_decrease.UseVisualStyleBackColor = true;
            this.btn_waterpic_decrease.Click += new System.EventHandler(this.btn_waterpic_decrease_Click);
            // 
            // btn_waterpic_increase
            // 
            this.btn_waterpic_increase.Location = new System.Drawing.Point(101, 152);
            this.btn_waterpic_increase.Name = "btn_waterpic_increase";
            this.btn_waterpic_increase.Size = new System.Drawing.Size(75, 23);
            this.btn_waterpic_increase.TabIndex = 5;
            this.btn_waterpic_increase.Text = "增大\r\n";
            this.btn_waterpic_increase.UseVisualStyleBackColor = true;
            this.btn_waterpic_increase.Click += new System.EventHandler(this.btn_waterpic_increase_Click_1);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 157);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 12);
            this.label9.TabIndex = 4;
            this.label9.Text = "水印大小调整";
            // 
            // btn_addpic_preview
            // 
            this.btn_addpic_preview.Location = new System.Drawing.Point(20, 211);
            this.btn_addpic_preview.Name = "btn_addpic_preview";
            this.btn_addpic_preview.Size = new System.Drawing.Size(75, 33);
            this.btn_addpic_preview.TabIndex = 3;
            this.btn_addpic_preview.Text = "预览\r\n";
            this.btn_addpic_preview.UseVisualStyleBackColor = true;
            this.btn_addpic_preview.Click += new System.EventHandler(this.btn_addpic_preview_Click);
            // 
            // btn_waterpic_select
            // 
            this.btn_waterpic_select.Location = new System.Drawing.Point(29, 117);
            this.btn_waterpic_select.Name = "btn_waterpic_select";
            this.btn_waterpic_select.Size = new System.Drawing.Size(120, 29);
            this.btn_waterpic_select.TabIndex = 2;
            this.btn_waterpic_select.Text = "选择图片...\r\n";
            this.btn_waterpic_select.UseVisualStyleBackColor = true;
            this.btn_waterpic_select.Click += new System.EventHandler(this.btn_waterpic_select_Click);
            // 
            // btn_add_pic_water
            // 
            this.btn_add_pic_water.Location = new System.Drawing.Point(101, 211);
            this.btn_add_pic_water.Name = "btn_add_pic_water";
            this.btn_add_pic_water.Size = new System.Drawing.Size(75, 33);
            this.btn_add_pic_water.TabIndex = 1;
            this.btn_add_pic_water.Text = "开始添加\r";
            this.btn_add_pic_water.UseVisualStyleBackColor = true;
            this.btn_add_pic_water.Click += new System.EventHandler(this.btn_add_pic_water_Click);
            // 
            // pictureBox_waterpic
            // 
            this.pictureBox_waterpic.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox_waterpic.Image = global::csharp_图片水印工具.Properties.Resources.logo;
            this.pictureBox_waterpic.Location = new System.Drawing.Point(17, 13);
            this.pictureBox_waterpic.Name = "pictureBox_waterpic";
            this.pictureBox_waterpic.Size = new System.Drawing.Size(169, 98);
            this.pictureBox_waterpic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_waterpic.TabIndex = 0;
            this.pictureBox_waterpic.TabStop = false;
            // 
            // btn_color
            // 
            this.btn_color.Location = new System.Drawing.Point(765, 446);
            this.btn_color.Name = "btn_color";
            this.btn_color.Size = new System.Drawing.Size(88, 23);
            this.btn_color.TabIndex = 6;
            this.btn_color.Text = "颜色设置...\r\n";
            this.btn_color.UseVisualStyleBackColor = true;
            this.btn_color.Click += new System.EventHandler(this.btn_color_Click);
            // 
            // btn_font
            // 
            this.btn_font.Location = new System.Drawing.Point(765, 367);
            this.btn_font.Name = "btn_font";
            this.btn_font.Size = new System.Drawing.Size(88, 23);
            this.btn_font.TabIndex = 4;
            this.btn_font.Text = "字体设置...\r\n";
            this.btn_font.UseVisualStyleBackColor = true;
            this.btn_font.Click += new System.EventHandler(this.btn_font_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(834, 527);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(121, 19);
            this.progressBar1.TabIndex = 9;
            // 
            // colorDialog1
            // 
            this.colorDialog1.Color = System.Drawing.Color.Red;
            // 
            // fontDialog1
            // 
            this.fontDialog1.Color = System.Drawing.Color.Red;
            this.fontDialog1.Font = new System.Drawing.Font("Microsoft Sans Serif", 99.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(12, 162);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(700, 400);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslb_1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 590);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(973, 22);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslb_1
            // 
            this.tslb_1.Name = "tslb_1";
            this.tslb_1.Size = new System.Drawing.Size(167, 17);
            this.tslb_1.Text = "Designed by Nathan Zhang";
            // 
            // btn_clear_files
            // 
            this.btn_clear_files.Location = new System.Drawing.Point(686, 27);
            this.btn_clear_files.Name = "btn_clear_files";
            this.btn_clear_files.Size = new System.Drawing.Size(47, 52);
            this.btn_clear_files.TabIndex = 6;
            this.btn_clear_files.Text = "清除文件";
            this.btn_clear_files.UseVisualStyleBackColor = true;
            this.btn_clear_files.Click += new System.EventHandler(this.btn_clear_files_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(763, 534);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 11;
            this.label5.Text = "处理进度";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(739, 598);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 12);
            this.label6.TabIndex = 9;
            this.label6.Text = "最后更新日期：";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(854, 598);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 12;
            this.label7.Text = "2016-12-11";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 147);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 13;
            this.label8.Text = "图片预览";
            // 
            // openFileDialog_waterpic
            // 
            this.openFileDialog_waterpic.FileName = "openFileDialog2";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(52, 25);
            this.menuStrip1.TabIndex = 14;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 文件ToolStripMenuItem
            // 
            this.文件ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.退出ToolStripMenuItem});
            this.文件ToolStripMenuItem.Name = "文件ToolStripMenuItem";
            this.文件ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.文件ToolStripMenuItem.Text = "文件";
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.退出ToolStripMenuItem.Text = "退出";
            this.退出ToolStripMenuItem.Click += new System.EventHandler(this.退出ToolStripMenuItem_Click);
            // 
            // tb_filename_pretext
            // 
            this.tb_filename_pretext.Location = new System.Drawing.Point(883, 497);
            this.tb_filename_pretext.Name = "tb_filename_pretext";
            this.tb_filename_pretext.Size = new System.Drawing.Size(72, 21);
            this.tb_filename_pretext.TabIndex = 17;
            this.tb_filename_pretext.Text = "w_";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(107, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 12);
            this.label2.TabIndex = 18;
            this.label2.Text = "双击可进行预览！\r\n";
            // 
            // chb_overwrite
            // 
            this.chb_overwrite.AutoSize = true;
            this.chb_overwrite.Enabled = false;
            this.chb_overwrite.Location = new System.Drawing.Point(769, 475);
            this.chb_overwrite.Name = "chb_overwrite";
            this.chb_overwrite.Size = new System.Drawing.Size(84, 16);
            this.chb_overwrite.TabIndex = 19;
            this.chb_overwrite.Text = "删除原文件\r\n";
            this.chb_overwrite.UseVisualStyleBackColor = true;
            // 
            // chb_addrename
            // 
            this.chb_addrename.AutoSize = true;
            this.chb_addrename.Checked = true;
            this.chb_addrename.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chb_addrename.Enabled = false;
            this.chb_addrename.Location = new System.Drawing.Point(769, 499);
            this.chb_addrename.Name = "chb_addrename";
            this.chb_addrename.Size = new System.Drawing.Size(108, 16);
            this.chb_addrename.TabIndex = 20;
            this.chb_addrename.Text = "文件名加前缀：\r\n";
            this.chb_addrename.UseVisualStyleBackColor = true;
            // 
            // btn_font_increase
            // 
            this.btn_font_increase.Location = new System.Drawing.Point(893, 396);
            this.btn_font_increase.Name = "btn_font_increase";
            this.btn_font_increase.Size = new System.Drawing.Size(44, 23);
            this.btn_font_increase.TabIndex = 21;
            this.btn_font_increase.Text = "增大";
            this.btn_font_increase.UseVisualStyleBackColor = true;
            this.btn_font_increase.Click += new System.EventHandler(this.btn_font_increase_Click);
            // 
            // btn_font_decrease
            // 
            this.btn_font_decrease.Location = new System.Drawing.Point(893, 425);
            this.btn_font_decrease.Name = "btn_font_decrease";
            this.btn_font_decrease.Size = new System.Drawing.Size(44, 23);
            this.btn_font_decrease.TabIndex = 22;
            this.btn_font_decrease.Text = "减小";
            this.btn_font_decrease.UseVisualStyleBackColor = true;
            this.btn_font_decrease.Click += new System.EventHandler(this.btn_font_decrease_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(767, 416);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 23;
            this.label11.Text = "字体大小：";
            // 
            // tb_font_size
            // 
            this.tb_font_size.Location = new System.Drawing.Point(837, 407);
            this.tb_font_size.Name = "tb_font_size";
            this.tb_font_size.Size = new System.Drawing.Size(40, 21);
            this.tb_font_size.TabIndex = 24;
            this.tb_font_size.Text = "100";
            this.toolTip1.SetToolTip(this.tb_font_size, "回车立即进行预览");
            this.tb_font_size.TextChanged += new System.EventHandler(this.tb_font_size_TextChanged);
            this.tb_font_size.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_font_size_KeyPress);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(806, 560);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(113, 12);
            this.linkLabel1.TabIndex = 25;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "http://zybsoft.com";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tb_right_down);
            this.groupBox1.Controls.Add(this.rb_right_up);
            this.groupBox1.Controls.Add(this.rb_left_down);
            this.groupBox1.Controls.Add(this.rb_left_up);
            this.groupBox1.Location = new System.Drawing.Point(518, 174);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 80);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "位置";
            this.groupBox1.Visible = false;
            // 
            // tb_right_down
            // 
            this.tb_right_down.AutoSize = true;
            this.tb_right_down.Location = new System.Drawing.Point(92, 55);
            this.tb_right_down.Name = "tb_right_down";
            this.tb_right_down.Size = new System.Drawing.Size(47, 16);
            this.tb_right_down.TabIndex = 3;
            this.tb_right_down.Text = "右下";
            this.tb_right_down.UseVisualStyleBackColor = true;
            // 
            // rb_right_up
            // 
            this.rb_right_up.AutoSize = true;
            this.rb_right_up.Location = new System.Drawing.Point(92, 20);
            this.rb_right_up.Name = "rb_right_up";
            this.rb_right_up.Size = new System.Drawing.Size(47, 16);
            this.rb_right_up.TabIndex = 2;
            this.rb_right_up.Text = "右上";
            this.rb_right_up.UseVisualStyleBackColor = true;
            // 
            // rb_left_down
            // 
            this.rb_left_down.AutoSize = true;
            this.rb_left_down.Location = new System.Drawing.Point(12, 55);
            this.rb_left_down.Name = "rb_left_down";
            this.rb_left_down.Size = new System.Drawing.Size(47, 16);
            this.rb_left_down.TabIndex = 1;
            this.rb_left_down.Text = "左下";
            this.rb_left_down.UseVisualStyleBackColor = true;
            // 
            // rb_left_up
            // 
            this.rb_left_up.AutoSize = true;
            this.rb_left_up.Checked = true;
            this.rb_left_up.Location = new System.Drawing.Point(12, 20);
            this.rb_left_up.Name = "rb_left_up";
            this.rb_left_up.Size = new System.Drawing.Size(47, 16);
            this.rb_left_up.TabIndex = 0;
            this.rb_left_up.TabStop = true;
            this.rb_left_up.Text = "左上";
            this.rb_left_up.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tb_y_position);
            this.groupBox2.Controls.Add(this.tb_x_position);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Location = new System.Drawing.Point(761, 318);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 43);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "位置";
            // 
            // tb_y_position
            // 
            this.tb_y_position.Location = new System.Drawing.Point(141, 10);
            this.tb_y_position.Name = "tb_y_position";
            this.tb_y_position.Size = new System.Drawing.Size(42, 21);
            this.tb_y_position.TabIndex = 27;
            this.tb_y_position.Text = "20";
            this.tb_y_position.TextChanged += new System.EventHandler(this.tb_y_position_TextChanged);
            this.tb_y_position.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_y_position_KeyPress);
            // 
            // tb_x_position
            // 
            this.tb_x_position.Location = new System.Drawing.Point(73, 10);
            this.tb_x_position.Name = "tb_x_position";
            this.tb_x_position.Size = new System.Drawing.Size(33, 21);
            this.tb_x_position.TabIndex = 26;
            this.tb_x_position.Text = "20";
            this.tb_x_position.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_x_position_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(112, 13);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(23, 12);
            this.label13.TabIndex = 25;
            this.label13.Text = "Y：";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 12);
            this.label12.TabIndex = 24;
            this.label12.Text = "坐标X：";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 612);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.tb_font_size);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.btn_font_decrease);
            this.Controls.Add(this.btn_font_increase);
            this.Controls.Add(this.chb_addrename);
            this.Controls.Add(this.chb_overwrite);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_filename_pretext);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btn_color);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btn_font);
            this.Controls.Add(this.btn_clear_files);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lb_files);
            this.Controls.Add(this.btn_folder_add);
            this.Controls.Add(this.btn_file_add);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "图片水印工具";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.拍摄日期.ResumeLayout(false);
            this.拍摄日期.PerformLayout();
            this.文字水印.ResumeLayout(false);
            this.文字水印.PerformLayout();
            this.图片水印.ResumeLayout(false);
            this.图片水印.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_waterpic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_file_add;
        private System.Windows.Forms.Button btn_folder_add;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ListBox lb_files;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage 文字水印;
        private System.Windows.Forms.TabPage 图片水印;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.Button btn_font;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button btn_color;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslb_1;
        private System.Windows.Forms.Button btn_clear_files;
        private System.Windows.Forms.Button btn_add_water;
        private System.Windows.Forms.Button btn_date;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TabPage 拍摄日期;
        private System.Windows.Forms.Button btn_pic_addshootdate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_photo_indate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox_waterpic;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.OpenFileDialog openFileDialog_waterpic;
        private System.Windows.Forms.Button btn_add_pic_water;
        private System.Windows.Forms.Button btn_waterpic_select;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 文件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
        private System.Windows.Forms.TextBox tb_filename_pretext;
        private System.Windows.Forms.Button btn_pic_preview;
        private System.Windows.Forms.Button btn_adddate_preview;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chb_overwrite;
        private System.Windows.Forms.CheckBox chb_addrename;
        private System.Windows.Forms.Button btn_addpic_preview;
        private System.Windows.Forms.Button btn_waterpic_decrease;
        private System.Windows.Forms.Button btn_waterpic_increase;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tb_waterpic_rate;
        private System.Windows.Forms.Button btn_font_increase;
        private System.Windows.Forms.Button btn_font_decrease;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tb_font_size;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton tb_right_down;
        private System.Windows.Forms.RadioButton rb_right_up;
        private System.Windows.Forms.RadioButton rb_left_down;
        private System.Windows.Forms.RadioButton rb_left_up;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tb_y_position;
        private System.Windows.Forms.TextBox tb_x_position;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
    }
}

