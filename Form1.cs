﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Text; 

namespace csharp_图片水印工具
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }



        private void Form1_Load(object sender, EventArgs e)
        {

            tabControl1.SelectedIndex = 0;
            pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            richTextBox1.Text = DateTime.Now.ToShortDateString();    //2011-6-9
            richTextBox1.SelectAll();
            richTextBox1.SelectionFont = fontDialog1.Font;
            richTextBox1.SelectionColor = colorDialog1.Color;
            //richTextBox1.Text = DateTime.Now.ToString("yyyy-MM-dd");   //2011-06-09
        }

        private void btn_font_Click(object sender, EventArgs e)
        {
           if(DialogResult .OK == fontDialog1.ShowDialog())
          {
            richTextBox1.SelectAll();
            richTextBox1.SelectionFont = 
                new Font(fontDialog1.Font.FontFamily, fontDialog1.Font.Size,fontDialog1.Font .Style );
           }
        }

        private void btn_color_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == colorDialog1.ShowDialog())
            {
                richTextBox1.SelectAll();
                richTextBox1.SelectionColor = colorDialog1.Color;
                tb_photo_indate.ForeColor = colorDialog1.Color;
            }
        }

       // public enum ext_name { gif, jpg, png, GIF, JPG, PNG }   //2使用枚举

       // Type ext = typeof(ext_name);     //2使用枚举

        private void btn_file_add_Click(object sender, EventArgs e)
        {
            System.Text.RegularExpressions.Regex rg = 
                new System.Text.RegularExpressions.Regex("(.jpg|.gif|.png|.JPG|.GIF|.PNG)$");

            //string[] ext = new string[] { ".jpg", ".bmp", ".gif", ".JPG", ".BMP", ".GIF" };   //1使用字符串数组

            if (DialogResult.OK == openFileDialog1.ShowDialog())
            {
                foreach (String file in openFileDialog1.FileNames)
                {
                    FileInfo fi = new FileInfo(file);
                    string file_ext = Path.GetExtension(fi.FullName);

                    //  foreach (string s in ext)  if (file_ext == s)  lib_files.items.Add(s_file.FullName);  //1使用字符串数组
                    //   foreach (string s in Enum.GetNames(ext))  if (file_ext =="."+ s)  lb_files.Items.Add(fi.FullName);  //2使用枚举
                    if (rg.Match(fi.FullName).Success == true) lb_files.Items.Add(fi.FullName);   //使用正则

                }
                lb_files.SelectedIndex = 0;
                tb_photo_indate.Text = get_photo_date(lb_files.Text);
            }

        }

        private void btn_folder_add_Click(object sender, EventArgs e)
        {
            System.Text.RegularExpressions.Regex rg = 
                new System.Text.RegularExpressions.Regex("(.jpg|.gif|.png|.JPG|.GIF|.PNG)$");

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                DirectoryInfo dinfo = new DirectoryInfo(folderBrowserDialog1.SelectedPath);

                foreach (FileInfo fi in dinfo.GetFiles())   //GetFiles(string Path,string "*.txt")
                {
               // string file_ext = Path.GetExtension(s_file.FullName);

                //foreach (string s in Enum.GetNames(ext))    if (file_ext =="."+s)        lb_files.Items.Add(s_file.FullName);
                if (rg.Match(fi.FullName).Success == true) lb_files.Items.Add(fi.FullName);   //使用正则

                }
            }
        }

        private void btn_clear_files_Click(object sender, EventArgs e)
        {
            lb_files.Items.Clear();
        }


//-----------------------------------------------------------------------------------------------------------------------

     # region 添加自定义文本水印 
        private void btn_date_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = DateTime.Now.ToShortDateString();
        }




        private void btn_add_water_Click(object sender, EventArgs e)  //自定义水印文字
        {
            if (checkfile() == false) return;  //检查是否有要处理的图片

            if (richTextBox1.Text == "")
            {
                MessageBox.Show("请设置水印文字！");
                return;
            }
            if (tb_filename_pretext .Text  == "")
            {
                MessageBox.Show("前缀不能为空!");
                return;
            }
            progressBar1.Value = 0;
            progressBar1 .Maximum =lb_files .Items .Count ;

            for(int i=0;i<lb_files .Items .Count ;i++)
            {
             lb_files.SelectedIndex = i;
             string file = lb_files.Items[i].ToString();  
             string filepath = Path.GetDirectoryName(file);   //路径名 如:  D:\精选照nathan
             string filename = Path.GetFileName(file);   //文件名包含扩展名 如: DSC00172.jpg
             string extension = Path.GetExtension(file); //返回文件扩展名 如:  .jpg

             //Invoke(new EventHandler(delegate { lb_cur_file.Text = file; }));
            // lb_cur_file.Text = file;  //?无法进行更新

                System.Drawing.Image image = System.Drawing.Image.FromFile(lb_files .Items [i].ToString ());

                Graphics g = Graphics.FromImage(image);
                g.DrawImage(image, 0, 0, image.Width, image.Height);  //填充原图
                Font f = new Font("Verdana", image.Width / 50);  //按照图片高度的1/50的字体大小显示
                Brush b = new SolidBrush( colorDialog1 .Color);  //获取颜色
                string addText = richTextBox1.Text.Trim();

                draw_txt_water(file, tb_filename_pretext .Text ,  richTextBox1.Text.Trim(), colorDialog1 .Color , fontDialog1.Font ,
                               tb_x_position .Text ,tb_y_position .Text ,  chb_overwrite .Checked  );

                progressBar1.Value += 1;   //进度条显示
            }
            MessageBox.Show("已完成所有图片加水印！");
        }



   #endregion

 //-----------------------------------------------------------------------------------------------------------------------

     # region 添加拍摄日期水印
        private void panel1_Click(object sender, EventArgs e)  //设置拍摄日期水印颜色
        {
            colorDialog1.ShowDialog();
            tb_photo_indate.ForeColor = colorDialog1.Color;
        }


        private void btn_pic_addshootdate_Click_1(object sender, EventArgs e)  //添加拍摄日期水印
        {
            if (checkfile() == false) return;  //检查是否有要处理的图片

            progressBar1.Maximum = lb_files.Items.Count;
            progressBar1.Value = 0;

            pictureBox1.ImageLocation = "";

            for (int i = 0; i < lb_files.Items.Count; i++)
            {
                lb_files.SelectedIndex = i;
                string file = lb_files.Items[i].ToString();
                string filepath = Path.GetDirectoryName(file);   //路径名 如:  D:\精选照nathan
                string filename = Path.GetFileName(file);   //文件名包含扩展名 如: DSC00172.jpg
                string extension = Path.GetExtension(file); //返回文件扩展名 如:  .jpg

                draw_txt_water(file,                          //原图
                               tb_filename_pretext.Text,      //文件名加前缀
                               get_photo_date(file),          //添加照片日期
                               colorDialog1 .Color,          //文字颜色
                               fontDialog1.Font,             //字体
                                tb_x_position.Text,
                                tb_y_position.Text, 
                               chb_overwrite .Checked);      //是否保留原文件

                progressBar1.Value += 1;   //进度条显示

           }
            MessageBox.Show("已处理完所有图片");
            System.Diagnostics.Process.Start(Path.GetDirectoryName (lb_files .Items[0].ToString ()));
        }
    #endregion
//-----------------------------------------------------------------------------------------------------------------------

        # region 添加图片水印 
        private void btn_waterpic_select_Click(object sender, EventArgs e)  //选择水印图片
        {
            if (DialogResult.OK == openFileDialog_waterpic.ShowDialog())
            {
                pictureBox_waterpic.ImageLocation = openFileDialog_waterpic.FileName;
            }
        }

        private void btn_add_pic_water_Click(object sender, EventArgs e)  //添加图片水印
        {
            if (checkfile() == false) return;  //检查是否有要处理的图片
            progressBar1.Maximum = lb_files.Items.Count;
            progressBar1.Value = 0;
            for (int i = 0; i < lb_files.Items.Count; i++)
            {
                lb_files.SelectedIndex = i;
                string file = lb_files.Items[i].ToString();
                string filepath = Path.GetDirectoryName(file);   //路径名 如:  D:\精选照nathan
                string filename = Path.GetFileName(file);   //文件名包含扩展名 如: DSC00172.jpg
                string extension = Path.GetExtension(file); //返回文件扩展名 如:  .jpg

                draw_pic_water(
                               file,
                               tb_filename_pretext .Text,
                               pictureBox_waterpic.Image,
                               Convert .ToInt16 (tb_waterpic_rate .Text),
                               tb_x_position .Text ,
                               tb_y_position .Text 
                               );


                progressBar1.Value += 1;   //进度条显示

            }
            MessageBox.Show("已处理完所有图片");
            System.Diagnostics.Process.Start(Path .GetDirectoryName (  lb_files.Text));
        }


        #endregion
//-------------------------------------------------------------------------------------------------------------------------
        #region 通用水印处理函数
        private string get_photo_date(string filename)
        {
            string str_date="";
            System.Drawing.Image image = System.Drawing.Image.FromFile(filename);

            try
            {
                for (int j = 0; j < image.PropertyItems.Length; j++)
                {
                    if (image.PropertyItems[j].Id == 0x0132)
                    {
                        //拍照日期
                        string value = Encoding.ASCII.GetString(image.PropertyItems[j].Value);
                        string[] dtParts = value.Split(new string[2] { " ", ":" }, StringSplitOptions.RemoveEmptyEntries);
                        if (Convert.ToInt32(dtParts[0]) > 0)
                        {
                            //有拍照日期
                            //DateTime shootingDate = new DateTime(Convert.ToInt32(dtParts[0]), Convert.ToInt32(dtParts[1]), Convert.ToInt32(dtParts[2]),
                            //      Convert.ToInt32(dtParts[3]), Convert.ToInt32(dtParts[4]), Convert.ToInt32(dtParts[5]), DateTimeKind.Local);
                            str_date = dtParts[0] + "年" + dtParts[1] + "月" + dtParts[2] + "日  " + dtParts[3] + ":" + dtParts[4] + ":" + dtParts[5];
                        }

                    }
                }
            }
            catch
            {
                //MessageBox.Show(filename + "没有内含正确的日期！");    
               
            }
            return str_date;
        }


        private bool draw_txt_water(string file, string fileaddpretext,string addText, Color color, Font font,
                                    string pos_x,string pos_y, bool delete_file) //添加文本水印函数
        {
           try
           {
                Image image = Image.FromFile(file);

                Graphics g = Graphics.FromImage(image);
                g.DrawImage(image, 0, 0, image.Width, image.Height);  //填充原图
                Brush b = new SolidBrush(color);  //获取颜色

                g.DrawString(addText, font, b, Convert.ToInt16 (pos_x ),Convert .ToInt16 (pos_y ));  //在原图中加上水印文字
                g.Dispose();

                string filepath = Path.GetDirectoryName(file);   //路径名 如:  D:\精选照nathan
                string filename = Path.GetFileName(file);   //文件名包含扩展名 如: DSC00172.jpg
                string extension = Path.GetExtension(file); //返回文件扩展名 如:  .jpg

                string newPath = filepath + "\\" + fileaddpretext + filename;  //按新文件名进行保存 如 D:\精选照nathan\DSC00172_water.jpg

                image.Save(newPath);
                image.Dispose();

                if (delete_file == true)           //delete oringin file,there is a problem here,the file can't be delected
                {
                    FileInfo fi = new FileInfo(file);
                    fi.Delete();
                }

                return true;
            }
           catch
          { return false; }
        }

       private bool draw_pic_water(string file,string file_pretext, Image ima,Int16 rate,string pos_x,string pos_y)  //添加图片水印函数
       {
           try
           {
               System.Drawing.Image image = System.Drawing.Image.FromFile(file);

               Graphics g = Graphics.FromImage(image);
               g.DrawImage(image, 0, 0, image.Width, image.Height);  //填充原图
               g.DrawImage(ima, Convert .ToInt16 (pos_x ), Convert .ToInt16 (pos_y ), ima.Width*rate/100, ima.Height*rate/100);  //填充水印
               g.Dispose();

               string filepath = Path.GetDirectoryName(file);   //路径名 如:  D:\精选照nathan
               string filename = Path.GetFileName(file);   //文件名包含扩展名 如: DSC00172.jpg
               string extension = Path.GetExtension(file); //返回文件扩展名 如:  .jpg

               string newPath = filepath + "\\" +file_pretext + filename ;  //按新文件名进行保存 如 D:\精选照nathan\DSC00172_water.jpg

               image.Save(newPath);
               image.Dispose();
               return true;
           }
           catch
           { return false; }
       }

     #endregion

     private void btn_addtxt_preview_Click(object sender, EventArgs e)
       {

         if (checkfile() == false) return;  //检查是否有要处理的图片

         if (lb_files.SelectedIndex <0) lb_files.SelectedIndex = 0;    
           System.Drawing.Image image = System.Drawing.Image.FromFile(lb_files .Text );

               Graphics g = Graphics.FromImage(image);
               g.DrawImage(image, 0, 0, image.Width, image.Height);  //填充原图

               Brush b = new SolidBrush(colorDialog1 .Color );  //获取颜色
               g.DrawString(richTextBox1.Text , fontDialog1 .Font , b, 
                   Convert .ToInt16 (tb_x_position .Text ),Convert .ToInt16(tb_y_position .Text ));  
         //在原图中加上水印文字

               g.Dispose();

               pictureBox1.Image = image;
       }

       private void btn_adddate_preview_Click(object sender, EventArgs e)
       {
           
           if (checkfile() == false) return;  //检查是否有要处理的图片
           
           if (lb_files .Items.Count ==0)
           {
               MessageBox.Show("请先添加图片！");
               return;
           }

           
           if (lb_files.SelectedIndex < 0) lb_files.SelectedIndex = 0;

           tb_photo_indate.Text = get_photo_date(lb_files .Text );


           System.Drawing.Image image = System.Drawing.Image.FromFile(lb_files.Text);

           Graphics g = Graphics.FromImage(image);
           g.DrawImage(image, 0, 0, image.Width, image.Height);  //填充原图

           Brush b = new SolidBrush(colorDialog1.Color);  //获取颜色
           g.DrawString(tb_photo_indate .Text , fontDialog1.Font, b,
                          Convert .ToInt16 ( tb_x_position .Text) ,Convert .ToInt16 (  tb_y_position .Text)); 
           //在原图中加上水印文字

           g.Dispose();

           pictureBox1.Image = image;
       }

       private void lb_files_MouseDoubleClick(object sender, MouseEventArgs e)
       {
           pictureBox1.ImageLocation = lb_files.Text;
           tb_photo_indate.Text = get_photo_date(lb_files.Text);
       }

       private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
       {
           Close();
       }

       private void btn_addpic_preview_Click(object sender, EventArgs e)
       {
           if (checkfile() == false) return;  //检查是否有要处理的图片

           Image image = Image.FromFile(lb_files.Text);

           Graphics g = Graphics.FromImage(image);
           g.DrawImage(image, 0, 0, image.Width, image.Height);  //填充原图


           g.DrawImage(pictureBox_waterpic.Image,
                        Convert .ToInt16 (tb_x_position .Text ),Convert.ToInt16 (tb_y_position .Text ), 
                        pictureBox_waterpic.Image.Width * (Convert .ToInt16 (tb_waterpic_rate.Text  ))/100,
                        pictureBox_waterpic.Image.Height * (Convert.ToInt16(tb_waterpic_rate.Text)) / 100);  //填充水印
           pictureBox1.Image = image;
           g.Dispose();
       }

       private void btn_waterpic_increase_Click(object sender, EventArgs e)
       {
           tb_waterpic_rate.Text = Convert.ToString(Convert.ToInt16(tb_waterpic_rate.Text) + 1);
       }

       private void btn_waterpic_increase_Click_1(object sender, EventArgs e)
       {
           tb_waterpic_rate.Text = Convert.ToString(Convert.ToInt16(tb_waterpic_rate.Text) + 1);
       }

       private void btn_waterpic_decrease_Click(object sender, EventArgs e)
       {
           if ( Convert.ToInt16(tb_waterpic_rate.Text) > 0 )
           tb_waterpic_rate.Text = Convert.ToString(Convert.ToInt16(tb_waterpic_rate.Text) - 1);
       }

       private void tb_waterpic_rate_KeyPress(object sender, KeyPressEventArgs e)
       {
           if ((e.KeyChar < (char)48 || e.KeyChar > (char)57) && e.KeyChar != (char)8)  //number input only
           {
               e.Handled = true;
           } 
          if (e.KeyChar ==13)
               btn_addpic_preview_Click(null, null);
       }

       private void btn_font_increase_Click(object sender, EventArgs e)
       {
           tb_font_size.Text = Convert .ToString( Convert .ToInt16 (tb_font_size .Text )+1);
       }

       private void btn_font_decrease_Click(object sender, EventArgs e)
       {
           tb_font_size.Text = Convert.ToString(Convert.ToInt16(tb_font_size.Text) - 1);
       }

       private void tb_font_size_KeyPress(object sender, KeyPressEventArgs e)
       {
           if ((e.KeyChar < (char)48 || e.KeyChar > (char)57) && e.KeyChar != (char)8)  //number input only
           {
               e.Handled = true;
           }
           if (e.KeyChar == 13)
               btn_adddate_preview_Click(null, null);
       }

       private void tb_font_size_TextChanged(object sender, EventArgs e)
       {
           fontDialog1.Font = new Font(fontDialog1.Font.Name, Convert.ToInt16(tb_font_size.Text), fontDialog1.Font.Style, fontDialog1.Font.Unit);
       }

       private void tb_x_position_KeyPress(object sender, KeyPressEventArgs e)
       {
           if ((e.KeyChar < (char)48 || e.KeyChar > (char)57) && e.KeyChar != (char)8)  //number input only
           {
               e.Handled = true;
           }
           if (e.KeyChar == 13)  //return key function
           {
             if (tabControl1 .SelectedIndex ==0)   btn_adddate_preview_Click(null, null);
             if (tabControl1.SelectedIndex == 1) btn_addtxt_preview_Click(null, null);
             if (tabControl1.SelectedIndex == 2) btn_addpic_preview_Click(null, null);
           }
       }

       private void tb_y_position_KeyPress(object sender, KeyPressEventArgs e)
       {
           if ((e.KeyChar < (char)48 || e.KeyChar > (char)57) && e.KeyChar != (char)8)  //number input only
           {
               e.Handled = true;
           }
           if (e.KeyChar == 13)
           {
               if (tabControl1.SelectedIndex == 0) btn_adddate_preview_Click(null, null);
               if (tabControl1.SelectedIndex == 1) btn_addtxt_preview_Click(null, null);
               if (tabControl1.SelectedIndex == 2) btn_addpic_preview_Click(null, null);
           }
       }

       private bool checkfile()
       {
           if (lb_files.Items.Count == 0) { MessageBox.Show("请先选择要进行处理的图片!", "提示 "); return false; }
           else return true;
       }

       private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
       {
           System.Diagnostics.Process.Start("http://zybsoft.com");
       }

       private void label13_Click(object sender, EventArgs e)
       {

       }

       private void tb_y_position_TextChanged(object sender, EventArgs e)
       {

       }
   }
}